  // How to change private instance data through constructor???
 class player{
	 private int jerNo=0;
	 private  String name =null;

	 player(int jerNo,String name){                                 //player(player this,int jerNo,String name) ,Here Hidden this reference present to store the address                                                                        // of the object and constructor initialize the instance variable through the this reference
		 System.out.println("Inside the Constructor");
                this.jerNo=jerNo;
		this.name = name;

	 }
          
	 void info(){
		 System.out.println(jerNo + "=" + name);
	 }

 }
 class Client{
	 public static void main(String[] args){               
	    

		 player obj;
		// obj.info();
	                                                           //------- Internal calls--------//
		 player obj1=new player(7,"MSD");                        //player(obj1,7,"MSD");
		 obj1.info();                                            //info(obj1);
		 player obj2=new player(18,"virat");                     //player(obj2,18,"virat");
		 obj2.info();                                             //info(obj2);
		 player obj3=new player(45,"Rohit");                     //player(obj3,45,"Rohit");
		 obj3.info();                                            //info(obj3);
	 
	 }
 }
