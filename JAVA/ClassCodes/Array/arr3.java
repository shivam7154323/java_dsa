                                 //Take IP as an Array Element from user with the help of Buffered Reader

 import java.io.*;
 class arr3{
	 public static void main(String[] args)throws IOException{
	    
	         BufferedReader obj = new BufferedReader(new InputStreamReader(System.in)); 

        	 System.out.println("Enter size of an Array");
	         int size=Integer.parseInt(obj.readLine());

	         int arr[]=new int[size];

	          System.out.println("Enter Array elements:");
		  for(int i=0;i<size;i++)
			  arr[i]=Integer.parseInt(obj.readLine());
	       
	       	  System.out.println("Array Elements are:");
		  for(int i=0;i<size;i++)
			  System.out.println(arr[i]);
	 }
 }






