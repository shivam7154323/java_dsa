                 // Take input as a Array Element from user with the help of Scanner Class......

import java.util.*;
class arr2 {
    public static void main(String[] args){
        Scanner sc= new Scanner(System.in);

        System.out.println("Enter size of an Array:");
        int size = sc.nextInt();

        int[] arr = new int[size];

        System.out.println("Enter Array elements:");
        for (int i = 0; i < size; i++) {
            arr[i] = sc.nextInt();
        }

        System.out.println("Array Elements are:");
        for (int i = 0; i < size; i++) {
            System.out.println(arr[i]);
        }
    }
}
