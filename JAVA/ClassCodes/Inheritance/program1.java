                   //Real time Example of Inheritance....

class ocean{
	ocean(){
		System.out.println("Inside Ocean Constructor:");
	}

	void oceanInfo(){
		System.out.println("OceanName:Indian Ocean");
	}
}

class sea extends ocean{
	sea(){
		System.out.println("Inside the Sea Constructor:");
	}

	void seaInfo(){
		System.out.println("SeaName:Arabian Sea");
	}
}

class river extends sea{
	river(){
                System.out.println("Inside the River Constructor:");
        }
}
class Water{
	public static void main(String[] args){
		river obj1=new river();
		obj1.seaInfo();

		sea obj2=new sea();
		obj2.oceanInfo();

		ocean obj3=new ocean();
		obj3.oceanInfo();

	} 
}



