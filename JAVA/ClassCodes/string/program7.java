       // Difference Between HashCode()  &  identityHashCode().........

class StringDemo{
	public static void main(String[]args){
		String str1="Shivam";                          //SCP
		String str2=new String("Shivam");              //Heap
		String str3="Shivam";                          //SCP
		String str4=new String("Shivam");              //Heap
	                             
	        System.out.println("Strings:");
		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);

	  

		System.out.println("identityHashCode() of Strings:");                      //identityHashCode() Deals with references....    
		System.out.println(System.identityHashCode(str1))		
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	
		System.out.println("HashCode() of Strings:");                              //hashCode() Deals with Values of string or content of string.....
	        System.out.println(str1.hashCode());
	        System.out.println(str2.hashCode());
	        System.out.println(str3.hashCode());
	        System.out.println(str4.hashCode());
	        
	}
}
