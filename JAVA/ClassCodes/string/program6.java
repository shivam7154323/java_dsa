       

class StringDemo{
	public static void main(String[]args){
		String str1="Shivam"; 
		String str2="Chincholkar";           
	        String str3= str1+str2;	                              //(+) operator internally called append() method of StringBuilder class...
		String str4=str1.concat(str2);


		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);

	//      System.out.println(System.identityHashCode(str1));
	//	System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
