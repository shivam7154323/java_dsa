     // string codes- 01/05/2023

class StringDemo{
	public static void main(String[]args){
		String str1="Core2Web";                                 //internally get spaced in SCP(String constant pull)
		String str2="Core2Web";
                 
                System.out.println(System.identityHashCode(str1));
                System.out.println(System.identityHashCode(str2));

		String str3="Bincaps";
		String str4=new String("Core2Web");
		String str5=new String("Core2Web");

                System.out.println(System.identityHashCode(str3));
                System.out.println(System.identityHashCode(str4));
                System.out.println(System.identityHashCode(str5));


	}
}

