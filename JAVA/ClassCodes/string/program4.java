            //We can write String in 3 Types....

class StringDemo{
	public static void main(String[]args){
		String str1="core2web"; 
		String str2=str1;                  
		String str3=new String("Shivam");          


		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
	}
}
