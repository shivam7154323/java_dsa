   /*  WAP to search specific elements from an array and return its index ....
       Input: 
              Enter size: 5
              Enter array elements :1 2 3 4 5
	      1st sinario:Enter Elements to search:4
	      2nd sinario:Enter Elements to search:55i
       

       Output:
              sinario 1st: Elements found in index position:3
	      sinario 2nd: Elements is not found
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		System.out.println("Enter Elements to search:");
		int ElementS=sc.nextInt();

		int result = search(arr,size,ElementS);
			if(result!=-1)
				System.out.println("Element found at Index:"+ result);
			else
				System.out.println("Element is not found in an array");
	}

	           static int search(int arr[],int size,int key){
			   for(int i=0;i<size;i++){
				   if(key==arr[i])
					   return i;
				   }
			   return -1;

		   };

	}


