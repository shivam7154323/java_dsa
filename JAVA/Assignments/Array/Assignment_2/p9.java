   /*  WAP to merge two given arrays.
       Input: 
              first array elements:44 4 34 5 3 7
              Enter array elements :1 2 3 4 5
	    
       Output:merge array :44 4 34 5 3 7 1 2 3 4 5
              
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of first Array:");
	        int size1=sc.nextInt();
		
		int arr1[]=new int[size1];
                
		System.out.println("Enter size of second Array:");
	        int size2=sc.nextInt();
		
		int arr2[]=new int[size2];

                System.out.println("Enter First Array Elements:");
		for(int i=0;i<arr1.length;i++)
			arr1[i]=sc.nextInt();
                
		System.out.println("Enter Second Array Elements:");
		for(int i=0;i<arr2.length;i++)
			arr2[i]=sc.nextInt();

		merge(arr1,arr2);
	}

	           static void merge(int arr1[],int arr2[]){
			   int arr3[]=new int[arr1.length+arr2.length];

                              for(int i=0;i<arr1.length;i++)
				      arr3[i]=arr1[i];
                               
			      int x=arr1.length;

                              for(int i=arr1.length;i<arr3.length;i++){
				      arr3[i]=arr2[i-x];
			      }
			   System.out.println("Merged array:");
			   for(int i=0;i<arr3.length;i++)
				   System.out.println(arr3[i]);
		   }


			   
			  


	}


