   /*  WAP to search specific elements from an array and return its index ....
       Input: 
              Enter size: 5
              Enter array elements :1 2 3 4 5
	    
       Output:Maximum Element :5
              
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		int result = max(arr,size);
			if(result==-1)
				System.out.println("All Elements in an array are Same:");
			else
				System.out.println("Maximum Element:"+ result);
	}

	           static int max(int arr[],int size){

				  int max=arr[0];
				   int count=0;

				   for(int i=0;i<size;i++){
				           if(max==arr[i])
					   count++;
				   }

			    if(count==size)
				    return -1;

			   for(int i=1;i<size;i++){
				   if(max < arr[i])
					   max=arr[i];
					  }
			   return max;
				   }
			  


	}


