   /*  WAP to print elements from an array and return its index ....
       Input: 
              Enter size: 5
              Enter array elements :1 2 3 4 5
	    
       Output:Minimum Element :1
              
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		int result = min(arr,size);
			if(result==-1)
				System.out.println("All Elements in an array are Same:");
			else
				System.out.println("Minimum Element:"+ result);
	}

	           static int min(int arr[],int size){

				  int min=arr[0];
				   int count=0;

				   for(int i=0;i<size;i++){
				           if(min==arr[i])
					   count++;
				   }

			    if(count==size)
				    return -1;

			   for(int i=1;i<size;i++){
				   if(min > arr[i])
					   min=arr[i];
					  }
			   return min;
				   }
			  


	}


