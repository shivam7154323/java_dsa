   /*  WAP to take size of array from user and also take interger elements from user Print Sum of even and odd elements ....
       Input: 
              Enter size: 5
              Enter array elements :1 2 3 4 5
       

       Output:
              Sum of Even Elements:6
	      Sum of Odd  Elements:9
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];
		int evensum=0;
		int oddsum=0;

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0)
				evensum=evensum+arr[i];
			else
				oddsum=oddsum+arr[i];

		}
		System.out.println("Sum of Even Elements:"+ evensum);
		System.out.println("Sum of Odd Elements:"+ oddsum);

	}
}
