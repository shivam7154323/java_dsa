   /*  WAP to print the elements whose addition  of digits is even.
       Input: 
              Enter size: 5
              Enter array elements :112 24 313 43 354
	    
       Output:  112 24 354
              
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class Program10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		     CheckEvensum(arr);
	}

	           static void CheckEvensum(int arr[]){
			   System.out.print("Array Element Whose addition of digits is even are:");
			   for(int i=0;i<arr.length;i++){
				   int temp=arr[i];
				   int sum=0;
				   int rem;
				   while(arr[i]!=0){
					   rem=arr[i]%10;
					   sum=sum+rem;
					   arr[i]=arr[i]/10;
					   if(arr[i]==0){
						   if(sum%2==0)
							   System.out.print(" "+ temp);
					   }



				   }
	
	}
	System.out.println();
		   }
}

