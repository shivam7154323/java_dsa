   /*  WAP to take size of array from user and also take interger elements from user Print count of even and odd elements ....
       Input: 
              Enter size: 5
              Enter array elements :1 2 3 4 5
       

       Output:
              No. of Even Elements:2
	      No. of Odd  Elements:3
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];
		int evencount=0;
		int oddcount=0;

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			if(arr[i]%2==0)
				evencount++;
			else
				oddcount++;

		}
		System.out.println("EvenCount:"+ evencount);
		System.out.println("OddCount:"+ oddcount);

	}
}
