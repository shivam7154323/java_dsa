   /*  WAP to take size of array from user and also take interger elements from user Print sum of all elements ....
       Input: 
              Enter size: 5
              Enter array elements :1 2 3 4 5
       

       Output:15 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];
		int sum=0;

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
			sum=sum+arr[i];

		}
		System.out.println("Sum of Elements in an Array is:"+ sum);

	}
}
