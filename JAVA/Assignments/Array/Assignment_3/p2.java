   /*  WAP to reverse each element in an array.....
       Input: 
              Enter size: 5
              Enter array elements :112 24 313 43 354
	    
       Output:211 42 313 34 453
              
   */    
import java.util.*;
class p2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		     reverse(arr);
	}
                          static void reverse(int arr[]){
			   System.out.print("Reverse Elements output:");
			   for(int i=0;i<arr.length;i++){
				   int rev=0;
				   while(arr[i]!=0){
					   int rem=arr[i]%10;
					   rev=rev*10+rem; 
					   arr[i]=arr[i]/10;
					   if(arr[i]==0){
							   System.out.print(" "+ rev);
					   }



				   }
	
	}
	System.out.println();
		   }
}

