   /*  WAP to print the second min  element in the array
       Input: 
              Enter size: 5
              Enter array elements :112 24 313 43 354
	    
       Output: 43
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/    
import java.util.*;
class p_10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		     SecMinEle(arr);
	}
                          static void SecMinEle(int arr[]){
				  int min=arr[0];
				       int secmin=0;
			   for(int i=1;i<arr.length;i++){
				 
				   if(min>arr[i]){
					   secmin=min;
					           min=arr[i];
				   }
					   if(min!=arr[i]){
						   if(secmin>arr[i])
							   secmin=arr[i];
					   }
					   
			   }
			   
			   System.out.println("Second min Element in Array:"  + secmin);
			   System.out.println("min Element in Array:"+ min);
			  }
}
	
		   

