   /*  WAP to print the second max  element in the array
       Input: 
              Enter size: 5
              Enter array elements :112 24 313 43 354
	    
       Output: 313
              
   */    
import java.util.*;
class p9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++)
			arr[i]=sc.nextInt();

		     SecMaxEle(arr);
	}
                          static void SecMaxEle(int arr[]){
				  int max=arr[0];
				       int secmax=0;
			   for(int i=1;i<arr.length;i++){
				 
				   if(max<arr[i]){
					   secmax=max;
					           max=arr[i];
				   }
					   if(max!=arr[i]){
						   if(secmax<arr[i])
							   secmax=arr[i];
					   }
					   
			   }
			   
			   System.out.println("Second max Element in Array:"  + secmax);
			   System.out.println("max Element in Array:"+ max);
			  }
}
	
		   

