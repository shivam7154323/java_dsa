   /*  WAP to take size of array from user and also take interger elements from user Print product of even elements only ....
       Input:    Enter size: 5
       Enter array elements :1 2 3 4 5
       

       Output:8  (2*4)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];
		int prod=1;

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();

			if(arr[i]%2==0)
			prod=prod*arr[i]; 

		}
		System.out.println("Product of Even Elements in an array is:"+ prod);
	}
}
