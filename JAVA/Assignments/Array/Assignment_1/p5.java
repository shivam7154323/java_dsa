   /*  WAP to take size of array from user and also take interger elements from user Print those elements which are divisible by 5 ....
       Input:    Enter size: 5
       Enter array elements :11 25 35 41 555
       

Output:25 35 555
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */    
import java.util.*;
class p5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

                System.out.println("Enter size of an Array:");
	        int size=sc.nextInt();
		
		int arr[]=new int[size];

                System.out.println("Enter an Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();

		}
		System.out.println("Elements which are divisible by 5 are:");	
		for(int i=0;i<arr.length;i++){

			if(arr[i]%5==0)
		         System.out.println(arr[i] +" ");

		}
	}
}
