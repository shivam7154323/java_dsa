           /* WAP take 7 character as an input, Print only vowels from the array ........
	     
	    Input: Enter characters- a b c @ r t e A U
	    Output:vowels are- a e A U
	    
------------------------------------------------------------------------------------------------------------------------------------------------------------------	   
	   */
import java.util.*;
class p4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size of an array:");
                int size = sc.nextInt();

		char arr[]=new char[size];
		
		System.out.println("Enter charcter as an array elements:");
		for(int i=0;i<size;i++)
			arr[i]=sc.next().charAt(0);
	                           
		System.out.println("Voweles in an array are:");
		for(int i=0;i<size;i++){
			if(arr[i]=='a' || arr[i]=='e' ||arr[i]=='i' ||arr[i]=='o' ||arr[i]=='u' ||arr[i]=='A' || arr[i]=='E' ||arr[i]=='I' ||arr[i]=='O' || arr[i]=='U')
				System.out.println(arr[i]);
			}
		}
}

		

