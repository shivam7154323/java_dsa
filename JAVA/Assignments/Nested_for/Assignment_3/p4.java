/*wap to print
 3C 3C 3C 3C
 3C 3C 3C
 3C 3C 
 3C 
 */
class Demo{
    public static void main(String[] args) {
        for(int i=1; i<=4; i++){
            for(int j=i;j<=4;j++){      // OR --> for(int j=1;j<=5-i;j++)
                System.out.print("3C ");
            }
            System.out.println();
        }
    }
 }