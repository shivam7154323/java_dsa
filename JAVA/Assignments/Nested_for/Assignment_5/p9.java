/*wap to take a number as input and print the addition of factorial of each digits from that number*/

import java.io.*;

class Demo{
    public static void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter num:");
        int num=Integer.parseInt(br.readLine());
        int sum=0;
        while(num!=0){
            int fact=1;
            int n=num%10;
            for(int i=1;i<=n;i++){
            fact=fact*i;
            }
            sum=sum+fact;
            num=num/10;
        }
        System.out.println(sum);
    }
}
