/*wap to print
  D4 C3 B2 A1
  A1 B2 C3 D4
  D4 C3 B2 A1
  A1 B2 C3 D4
 */

 import java.io.*;


 class Demo{
    public static void main(String[] args) throws IOException{
        BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter rows:");
        int rows=Integer.parseInt(br.readLine());

        for(int i=1;i<=rows;i++){
            char ch1='A',ch2='D';
            int num=4;
            for(int j=1;j<=4;j++){
                if(i%2==0){
                    System.out.print(ch1 + "" + j +"  ");
                    ch1++;
                    
                }else{
                    System.out.print(ch2 + "" + num + "  ");
                    ch2--;
                    num--;

                }
             }
            System.out.println();
        }

    }
 }