/*wap to print all even numbers in reverse order and odd numbers in the standard way.Both seperately
 * within the range take start and end forom from user 
 */

 import java.io.*;

 class Demo{
    public static void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter start:");
        int start=Integer.parseInt(br.readLine());
       
       
        System.out.println("enter end:");
        int end=Integer.parseInt(br.readLine());

        for(int i=1;i<=2;i++){
            if(i==1){
                for(int j=end;j>=start;j--){
                    if(j%2==0){
                        System.out.print(j+" ");
                        
                    }
                }
            }else{
                for(int j=start;j<=end;j++){
                    if(j%2==0){
                        System.out.print(j+" ");
                        
                    }
                }
            }
            System.out.println();
        }

    }
 }
