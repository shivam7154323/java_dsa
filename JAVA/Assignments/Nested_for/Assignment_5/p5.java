/*wap to print  row=4
 * 0
 * 1 1
 * 2 3 5
 * 8 13 21 34
 */

 import java.io.*;

class Demo{
    public static void main(String[] args) throws IOException{
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter rows:");
        int rows=Integer.parseInt(br.readLine());
        int a=1,b=1;
        for(int i=1;i<=rows;i++){
            for(int j=1;j<=i;j++){
            a=b-a;
            b=a+b;
            System.out.print(a+ " ");
        }
        System.out.println();
       }
}
}