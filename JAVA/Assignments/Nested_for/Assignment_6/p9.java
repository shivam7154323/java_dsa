/* 
  WAP to print the range of strong number between range entered by user 

 */
import java.io.*;
class Demo{
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in)); 

        System.out.println("enter start:");
        int start =Integer.parseInt(br.readLine());

        System.out.println("enter end:");
        int end =Integer.parseInt(br.readLine());

        for(int i= start;i<=end;i++ ){
            int sum=0;
            for(int j=i; j!=0 ;j=j/10){
                int fact=1;
                int rem=j%10;
                for(int k=1;k<=rem;k++){
                    fact=fact*k;
                }
                sum=sum+fact;
            }
            if(i==sum){
                System.out.println(i+" ");

            }
            
        }
    }
}
