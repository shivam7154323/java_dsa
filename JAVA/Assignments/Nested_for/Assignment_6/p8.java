/*WAP to take range as input form user and print palendrome number in that range*/
import java.io.*;
class Demo{
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in)); 

        System.out.println("enter start:");
        int start =Integer.parseInt(br.readLine());

        System.out.println("enter end:");
        int end =Integer.parseInt(br.readLine());
        for(int i=start;i<=end;i++){
            int num=i;
            int rev=0;
            for(int j=i;j!=0;j=j/10){
                int rem=j%10;
                rev=rev*10+rem;
            }
            if(rev==i){
                System.out.println(" "+i);
            }
        }
    }
}
