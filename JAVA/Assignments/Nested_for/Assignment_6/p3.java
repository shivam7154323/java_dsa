/*WAP to print the perfect squares between range .take the range from user */

import java.io.*;
class Demo{
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in)); 

        System.out.println("enter start:");
        int start =Integer.parseInt(br.readLine());

        System.out.println("enter end:");
        int end =Integer.parseInt(br.readLine());

        for(int i=start; i*i<=end ;i++){
            System.out.println(i*i +" ");
        }
    }
}
