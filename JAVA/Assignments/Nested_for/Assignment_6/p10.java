/*WAP to take range from the user and print Armstrong numbers  */

import java.io.*;
class Demo{
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in)); 

        System.out.println("enter start:");
        int start =Integer.parseInt(br.readLine());

        System.out.println("enter end:");
        int end =Integer.parseInt(br.readLine());
       

        for(int i= start;i<=end;i++ ){
            int count=0;
            for(int x=i; x!=0 ;x=x/10){
                count++;
            }
            int sum=0;
          for(int j=i;j!=0;j=j/10){
                int mul=1;
                int rem=j%10;
                for(int k=1;k<=count;k++){
                    mul=mul*rem;

                }
                sum=sum+mul;
            }
            if(i==sum){
                System.out.println(i+" ");

            }
            
        }
    }
}
