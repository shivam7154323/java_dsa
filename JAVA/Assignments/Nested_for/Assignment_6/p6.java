/*WAP to take 5 numbers as input from the user and print the count of digits in those numbers */

import java.io.*;
class Demo{
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in)); 
        System.out.println("Enter 5 numbers  :");

        for(int i=1 ; i<=5; i++){
            System.out.println("Number :" +i);
            int num=Integer.parseInt(br.readLine());
            int count=0;
            for(int j=num ;j!=0 ; j=j/10){
                count++;
            }
            System.out.println("The digit count in "+num +" is "+count);


        }

    }
}

